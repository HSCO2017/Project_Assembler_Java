/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assembler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Harry
 */
public class Parser {
    
    private BufferedReader reader;
    private String current;
    private String next;
    
    
    
    public Parser(File input) throws FileNotFoundException, IOException{
        FileReader r = new FileReader(input);
        reader = new BufferedReader(r);
        next=getNextLine();
    }
    
    private String getNextLine() throws IOException{
        next = reader.readLine();
        while ((next != null) && (next.isEmpty() || isComment(next))) {
            next = reader.readLine();
            if(next == null){
                return null;
            }
        }
        if(next != null){
            int comment = next.indexOf("//");
            if(comment != -1){
                next = next.substring(0, comment-1);
            }
        }        
        return next;
    }
    
    private boolean isComment(String line){
        return line.trim().startsWith("//");
    }
    
    public boolean hasMoreCommands(){
        return next != null;
    }
    
    public void advance() throws IOException{
        current = next;
        next = getNextLine();
    }
    
    public String commandType(){
        String line = current.trim();
        if (line.startsWith("(") && line.endsWith(")")) {
            return "L_COMMAND";
        }
        else if(line.startsWith("@")){
            return "A_COMMAND";
        }
        else{
            return "C_COMMAND";
        }
    }
    
    public String symbol(){
        String line = current.trim();
        if (commandType().equals("L_COMMAND")) {
            return line.substring(1, line.length()-1);
        }
        else if(commandType().equals("A_COMMAND")){
            return line.substring(1);
        }
        else{
            return null;
        }
    }
    
    public String dest(){
        String line = current.trim();
        int index = line.indexOf("=");
        
        if(index == -1){
            return null;
        }
        else{
            return  line.substring(0, index);
        }
    }
    
    public String comp(){
        String line = current.trim();
        int index = line.indexOf("=");
        if(index != -1){
            line = line.substring(index+1);            
        }
        
        index=line.indexOf(";");
        if (index == -1) {
            return line;
        }
        else{
            return  line.substring(0,index);
        }
    }
    
    public String jump(){
        String line = current.trim();
        int index = line.indexOf(";");
        
        if(index == -1){
            return null;
        }
        else{
            return line.substring(index+1);
        }
    }
    
    public void close() throws IOException{
        reader.close();
    }
}
