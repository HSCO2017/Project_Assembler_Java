/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assembler;

import java.util.Hashtable;

/**
 *
 * @author Harry
 */
public class Code {
    Hashtable<String, String> destMnemonic;
    Hashtable<String, String> compMnemonic;
    Hashtable<String, String> jumpMnemonic;
    
    public Code(){
        destMnemonic = new Hashtable<String, String>();
        compMnemonic = new Hashtable<String, String>();
        jumpMnemonic = new Hashtable<String, String>();
        fillDest();
        fillComp();
        fillJump();
    }
    
    private void fillDest(){
        destMnemonic.put("NULL", "000");
        destMnemonic.put("M", "001");
        destMnemonic.put("D", "010");
        destMnemonic.put("MD", "011");
        destMnemonic.put("A", "100");
        destMnemonic.put("AM", "101");
        destMnemonic.put("AD", "110");
        destMnemonic.put("AMD", "111");
    }
    
    private void fillComp(){
        compMnemonic.put("0", "0101010");
        compMnemonic.put("1", "0111111");
        compMnemonic.put("-1", "0111010");
        compMnemonic.put("D", "0001100");
        compMnemonic.put("A", "0110000");
        compMnemonic.put("M", "1110000");
        compMnemonic.put("!D", "0001101");
        compMnemonic.put("!A", "0110001");
        compMnemonic.put("!M", "1110001");
        compMnemonic.put("-D", "0001111");
        compMnemonic.put("-A", "0110011");
        compMnemonic.put("-M", "1110011");
        compMnemonic.put("D+1", "0011111");
        compMnemonic.put("A+1", "0110111");
        compMnemonic.put("M+1", "1110111");
        compMnemonic.put("D-1", "0001110");
        compMnemonic.put("A-1", "0110010");
        compMnemonic.put("M-1", "1110010");
        compMnemonic.put("D+A", "0000010");
        compMnemonic.put("D+M", "1000010");
        compMnemonic.put("D-A", "0010011");
        compMnemonic.put("D-M", "1010011");
        compMnemonic.put("A-D", "0000111");
        compMnemonic.put("M-D", "1000111");
        compMnemonic.put("D&A", "0000000");
        compMnemonic.put("D&M", "1000000");
        compMnemonic.put("D|A", "0010101");
        compMnemonic.put("D|M", "1010101");
    }
    
    private void fillJump(){
        jumpMnemonic.put("NULL", "000");
        jumpMnemonic.put("JGT", "001");
        jumpMnemonic.put("JEQ", "010");
        jumpMnemonic.put("JGE", "011");
        jumpMnemonic.put("JLT", "100");
        jumpMnemonic.put("JNE", "101");
        jumpMnemonic.put("JLE", "110");
        jumpMnemonic.put("JMP", "111");
    }
    
    public String destCode(String mnemonic){
        if(mnemonic == null || mnemonic.isEmpty()){
            mnemonic="NULL";
        }
        return destMnemonic.get(mnemonic);
    }
    
    public String compCode(String mnemonic){
        return compMnemonic.get(mnemonic);
    }
    
    public String jumpCode(String mnemonic){
        if(mnemonic == null || mnemonic.isEmpty()){
            mnemonic="NULL";
        }
        return jumpMnemonic.get(mnemonic);
    }
}

