/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assembler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Harry
 */
public class Assembler {

    /**
     * @param args the command line arguments
     */
    
    private static Code encoder;
    private static SymbolTable table;
    private static BufferedWriter writerFile;
    
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        System.out.println("HACK Assembler\nInput file address (File type .asm):");
        Scanner reader = new Scanner(System.in);
        String address = reader.next();
        try{
            if (address.length()==0) {
            System.out.println("No source file.");            
            }
            else
            {
                File inputFile = new File(address);
                boolean temp = fileExtension(inputFile);
                if (!inputFile.exists()) {
                    System.out.println("File doesn't exist.");            
                }
                else if (temp) {
                    String outFile = inputFile.getAbsolutePath();
                    outFile=outFile.replace(".asm", ".hack");

                    File outputFile = new File(outFile);
                    outputFile.createNewFile(); 
                    FileWriter w = new FileWriter(outputFile);
                    writerFile = new BufferedWriter(w);
                    encoder = new Code();
                    table = new SymbolTable();
                    labelFirstPass(inputFile);
                    parse(inputFile);
                    System.out.println("The file has been translated.");
                }
                else
                {
                    System.out.println("The file extension is not .asm");
                }
            }
        }
        catch(Exception e){
            System.out.println(e);
        }             

    }
    
    private static boolean fileExtension(File inputF){
        String outFileName = inputF.getName();
        int indexExtension = outFileName.lastIndexOf(".");
        String outFileNameE = outFileName.substring(indexExtension);
        return outFileNameE.equals(".asm");
    }
    
    private static void parse(File i) throws IOException{
        Parser parser = new Parser(i);
        while (parser.hasMoreCommands()) {            
            parser.advance();
            
            String commandType = parser.commandType();
            String command = null;
            
            if(commandType.equals("A_COMMAND")){
                String symbol = parser.symbol();
                boolean isSymbol = !Character.isDigit(symbol.charAt(0));
                String address;
                //SYMBOL IMPLEMENTATION
                if(isSymbol){
                    boolean exist= table.contains(symbol);
                    if(!exist){
                        int dataA = table.getDA();
                        table.addEntry(symbol, dataA);
                        table.addDA();
                    }
                    address = Integer.toString(table.getAddress(symbol));
                }else{
                    address = symbol;
                }              
                
                command = aCommand(address);
            }
            else if(commandType.equals("C_COMMAND")){
                String comp = parser.comp();
                String dest = parser.dest();
                String jump = parser.jump();
                command = cCommand(comp, dest, jump);
            }
            
            if(!commandType.equals("L_COMMAND")){
                writerFile.write(command);
                writerFile.newLine();
            }
        }
        
        parser.close();
        writerFile.flush();
        writerFile.close();
    }
    
    private static String aCommand(String address){
        int value = Integer.parseInt(address);
        String binaryNumber = Integer.toBinaryString(value);
        String formattedBinaryNumber =
                        String.format("%15s", binaryNumber).replace(' ', '0');
        return "0" + formattedBinaryNumber;
    }
    
    private static String cCommand(String comp, String dest, String jump){
        StringBuilder command = new StringBuilder();
        command.append("111");
        command.append(encoder.compCode(comp));
        command.append(encoder.destCode(dest));
        command.append(encoder.jumpCode(jump));
        return command.toString();
    }
    
    private static void labelFirstPass(File i) throws IOException{
        Parser parser = new Parser(i);
        while (parser.hasMoreCommands()) {            
            parser.advance();
            String commandType = parser.commandType();
            
            if(commandType.equals("L_COMMAND")){
                String symbol = parser.symbol();
                int address = table.getPA();
                table.addEntry(symbol, address);
            }
            else{
                table.addPA();
            }
        }
        parser.close();
    }
}
