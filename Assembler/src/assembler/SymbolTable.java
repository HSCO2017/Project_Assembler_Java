/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assembler;

import java.util.Hashtable;

/**
 *
 * @author Harry
 */
public class SymbolTable {
    private static final int DATA_START=16;
    private static final int DATA_ENDING=16384;
    private static final int PROGRAM_START=0;
    private static final int PROGRAM_ENDING=32767;
    
    private Hashtable<String, Integer> symbolTable;
    private int aD;
    private int aP;
    
    public SymbolTable(){
        aD = DATA_START;
        aP = PROGRAM_START;
        symbolTable = new Hashtable<String, Integer>();
        symbolTable.put("SP", 0);
        symbolTable.put("LCL", 1);
        symbolTable.put("ARG", 2);
        symbolTable.put("THIS", 3);
        symbolTable.put("THAT", 4);
        symbolTable.put("R0", 0);
        symbolTable.put("R1", 1);
        symbolTable.put("R2", 2);
        symbolTable.put("R3", 3);
        symbolTable.put("R4", 4);
        symbolTable.put("R5", 5);
        symbolTable.put("R6", 6);
        symbolTable.put("R7", 7);
        symbolTable.put("R8", 8);
        symbolTable.put("R9", 9);
        symbolTable.put("R10", 10);
        symbolTable.put("R11", 11);
        symbolTable.put("R12", 12);
        symbolTable.put("R13", 13);
        symbolTable.put("R14", 14);
        symbolTable.put("R15", 15);
        symbolTable.put("SCREEN", 16384);
        symbolTable.put("KBD", 24576);
    }
    
    public void addEntry(String symbol, int address){
        symbolTable.put(symbol, address);
    }
    
    public boolean contains(String symbol){
        return symbolTable.containsKey(symbol);
    }
    
    public int getAddress(String symbol){
        return symbolTable.get(symbol);
    }
    
    public void addDA(){
        aD++;
        if(aD>DATA_ENDING){
            System.out.println("Error: Ran out of memory space.");
        }
    }
    
    public void addPA(){
        aP++;
        if(aP>PROGRAM_ENDING){
            System.out.println("Error: Ran out of memory space.");
        }
    }
    
    public int getPA(){
        return aP;
    }
    
    public int getDA(){
        return aD;
    }
}
